# Project 2 
### Using the OCAPI  (Open Commerce API) Shop API with Node
 

  This application uses OCAPI calls to populate a site with category navigation as well as uses another OCAPI call to populate a product page based on that category.

# Quick Outline  
- EJS used for html templating 
    - Partials used for general template
    - Specific pages were made for each catergory (just in case you wanted special feature for each catergory)
- Bootstrap used for styling
- Simple Implemention of Javascript Promises for Chaining 

# Running the program  
```
 node server 
```
The server should broadcast on http://localhost:8080

# Author  
Timothy Field @ LYONSCG
with help from colleagues 
- Especially setting up Promises to help with ordering API calls
 
 
 # Thank You 