// Needed Modules
var express = require('express');
var app = express();
var request = require('request');
var Promise = require('promise');

//For console formatting
var lineBr = "\n---------------------------------------------------\n";

//Default API calls
var ocapiProductsURL = "http://lyons5.evaluation.dw.demandware.net/s/SiteGenesis/dw/shop/v17_4/product_search?q=game&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
let ocapiCategoryURL = "http://lyons5.evaluation.dw.demandware.net/s/SiteGenesis/dw/shop/v17_4/categories/root?levels=3&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

//Varibles to parse JSONs into
var categoryJSON = undefined;
var productJSON = undefined;
var productSeperateJSON = undefined;
var allProductsJSON = undefined;
var allProductsInfoJSON = undefined;

//-Main Object-  holds all product info and indexes those by category ID
var jsonCatIdIndexing = {}; 

//Arrays to put JS Objectss 
var jsonProductByCatArray = [];
var productIdArray = [];
var productObjsArray = [];
var categoryIDsArray = [];
var allProductsArray = [];
var allProductsInfo = [];

//Starts the Promise then chains requests down to rendering the webpages
var promise = new Promise(function (resolve, reject) {
	var req = request(ocapiCategoryURL, function (error, response, body) { //Get the category information
		console.log(lineBr);
		console.log("Beginning take off protocol...(Making Request for Category Info...)");
		if (!error && response.statusCode == 200) {
			categoryJSON = JSON.parse(body); 

			for (var i = 0; i < categoryJSON.categories.length; i++) { // go through categories like NewArrivals
				categoryIDsArray.push(categoryJSON.categories[i].id);
 				if (categoryJSON.categories[i].categories != null) {
					for (var x = 0; x < categoryJSON.categories[i].categories.length; x++) { // go through categories.categories Men, Women, Electronic
						categoryIDsArray.push(categoryJSON.categories[i].categories[x].id);
						if (categoryJSON.categories[i].categories[x].categories != null) {
							for (var j = 0; j < categoryJSON.categories[i].categories[x].categories.length; j++) { //go through categories.categories.catergoires Women-Clothing-Tops 
								categoryIDsArray.push(categoryJSON.categories[i].categories[x].categories[j].id);
							}
						}
					}
				}
			}
			//	console.log(categoryIDsArray); //Keep for Debug
			console.log(lineBr);
			console.log("Rocket Fuel has been filled... (API Request for Category Infomation Complete)");
			console.log(lineBr);
			resolve(1);
		
		}
 	});
});

promise.then(function (data) { //Then get the product information

	console.log("Contacting Houston... (Making Requests for Product Info)");
	console.log(lineBr);
	return new Promise(function (resolve,reject) {
	for (let z = 0; z < categoryIDsArray.length; z++) {
		let searchForProductURL = "http://lyons5.evaluation.dw.demandware.net/s/SiteGenesis//dw/shop/v18_1/product_search?refine=cgid=" + categoryIDsArray[z] + "&expand=prices,images&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

		request(searchForProductURL, function (error, response, body) {

			if (!error && response.statusCode == 200) {
				allProductsJSON = JSON.parse(body);
				jsonCatIdIndexing[categoryIDsArray[z] + ""] = allProductsJSON;
				allProductsArray.push(allProductsJSON);
			}
			for (let x = 0; x < allProductsArray.length; x++) {
				if (allProductsArray[x].hits != null) {
					for (let k = 0; k < allProductsArray[x].hits.length; k++) {
						let productURL = allProductsArray[x].hits[k].link;
						request(productURL, function (error, response, body) {
							if (!error && response.statusCode == 200) {
								allProductsInfoJSON = JSON.parse(body);
								allProductsInfo.push(allProductsInfoJSON);
							}


						});
					}

				}
			}
		});

	}
	resolve(1);
	});

	

}).then(function (data) { //Then render the pages once we have everything we need!
	
	console.log("Permission Granted... (API Request for Product Infomation Complete)");
	console.log(lineBr);

 		console.log("We're have confirmed lift off!");
		console.log("Fire her up captain!");
		console.log(lineBr);
		console.log('Broadcasting on port 8080' + lineBr);
	

 	var count = 0;

	//-- All pages --//
	//	Index page 
	app.get('/', function (req, res) {
		res.render('pages/index', {
			headerCat: categoryJSON,
			allProds: allProductsJSON
		})
	});

	// about page 
	app.get('/about', function (req, res) {
		res.render('pages/about', {
			headerCat: categoryJSON
		});
	});


	//----------------------NEW ARRIVALS----------------------//
	// newarrivals page 
	app.get('/newarrivals', function (req, res) {
		res.render('pages/newarrivals', {
			cgID: "newarrivals",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: 0,
			count2: 0,
			jsonProductByCatArray: jsonProductByCatArray,
			allProds: allProductsArray,
			allProds: allProductsArray,
			allProds_info: allProductsInfo,
			jsonCatIdIndexing: jsonCatIdIndexing
		});

	});
	
	// newarrivals women page 
	app.get('/newarrivals-womens', function (req, res) {
		res.render('pages/newarrivals-womens', {
			cgID: "newarrivals-womens",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonProductByCatArray: jsonProductByCatArray,
			allProds: allProductsArray,
			allProds: allProductsArray,
			allProds_info: allProductsInfo,
			jsonCatIdIndexing: jsonCatIdIndexing
		});
	});

	// newarrivals mens page 
	app.get('/newarrivals-mens', function (req, res) {
		res.render('pages/newarrivals-mens', {
			cgID: "newarrivals-mens",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: 0,
			count2: 0,
			jsonProductByCatArray: jsonProductByCatArray,
			allProds: allProductsArray,
			allProds: allProductsArray,
			allProds_info: allProductsInfo,
			jsonCatIdIndexing: jsonCatIdIndexing
		});
	});

	// newarrivals electronics page 
	app.get('/newarrivals-electronics', function (req, res) {
		res.render('pages/newarrivals-electronics', {
			cgID: "newarrivals-electronics",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonProductByCatArray: jsonProductByCatArray,
			allProds: allProductsArray,
			allProds: allProductsArray,
			allProds_info: allProductsInfo,
			jsonCatIdIndexing: jsonCatIdIndexing
		});
	});

	//----------------------WOMENS----------------------//
	//womens
	app.get('/womens', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: count,
			jsonCatIdIndexing: jsonCatIdIndexing
		});
	});

	//womens-clothing
	app.get('/womens-clothing', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-clothing",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//womens-outfits
	app.get('/womens-outfits', function (req, res) {
		res.render('pages/womens-outfits', {
			cgID: "womens-outfits",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: count,
			jsonCatIdIndexing: jsonCatIdIndexing
		});
	});

	//womens-clothing-tops
	app.get('/womens-clothing-tops', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-clothing-tops",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 1,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//womens-clothing-dresses
	app.get('/womens-clothing-dresses', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-clothing-dresses",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 2,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//womens-clothing-bottoms
	app.get('/womens-clothing-bottoms', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-clothing-bottoms",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 3,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//womens-clothing-jackets
	app.get('/womens-clothing-jackets', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-clothing-jackets",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 4,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//womens-clothing-jackets
	app.get('/womens-clothing-feeling-red', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-clothing-feeling-red",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 5,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//womens-jewelry
	app.get('/womens-jewelry', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-jewelry",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'womens-jewelry-earrings',
	app.get('/womens-jewelry-earrings', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-jewelry-earrings",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'womens-jewlery-bracelets',
	app.get('/womens-jewlery-bracelets', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-jewlery-bracelets",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'womens-jewelry-necklaces',
	app.get('/womens-jewelry-necklaces', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-jewelry-necklaces",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'womens-accessories',
	app.get('/womens-accessories', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-accessories",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'womens-accessories-scarves',
	app.get('/womens-accessories-scarves', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-accessories-scarves",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'womens-accessories-shoes',
	app.get('/womens-accessories-shoes', function (req, res) {
		res.render('pages/womens', {
			cgID: "womens-accessories-shoes",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//----------------------MENS----------------------//
	// 'mens',
	app.get('/mens', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'mens-clothing',
	app.get('/mens-clothing', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens-clothing",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'mens-clothing-suits',
	app.get('/mens-clothing-suits', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens-clothing-suits",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'mens-clothing-jackets',
	app.get('/mens-clothing-jackets', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens-clothing-jackets",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'mens-clothing-dress-shirts',
	app.get('/mens-clothing-dress-shirts', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens-clothing-dress-shirts",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'mens-clothing-shorts',
	app.get('/mens-clothing-shorts', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens-clothing-shorts",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'mens-clothing-pants',
	app.get('/mens-clothing-pants', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens-clothing-pants",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'mens-accessories',
	app.get('/mens-accessories', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens-accessories",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'mens-accessories-ties',
	app.get('/mens-accessories-ties', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens-accessories-ties",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'mens-accessories-gloves',
	app.get('/mens-accessories-gloves', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens-accessories-gloves",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'mens-accessories-luggage',
	app.get('/mens-accessories-luggage', function (req, res) {
		res.render('pages/mens', {
			cgID: "mens-accessories-luggage",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[2].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//----------------------ELECTRONICS----------------------//
	// electronics',
	app.get('/electronics', function (req, res) {
		res.render('pages/electronics', {
			cgID: "electronics",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});
	//   'electronics-televisions',
	app.get('/electronics-televisions', function (req, res) {
		res.render('pages/electronics', {
			cgID: "electronics-televisions",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//   'electronics-televisions-flat-screen',
	app.get('/electronics-televisions-flat-screen', function (req, res) {
		res.render('pages/electronics', {
			cgID: "electronics-televisions-flat-screen",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//   'electronics-televisions-projection',
	app.get('/electronics-televisions-projection', function (req, res) {
		res.render('pages/electronics', {
			cgID: "electronics-televisions-projection",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//   'electronics-digital-cameras',
	app.get('/electronics-digital-cameras', function (req, res) {
		res.render('pages/electronics', {
			cgID: "electronics-digital-cameras",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//   'electronics-digital-media-players',
	app.get('/electronics-digital-media-players', function (req, res) {
		res.render('pages/electronics', {
			cgID: "electronics-digital-media-players",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//   'electronics-gps-units',
	app.get('/electronics-gps-units', function (req, res) {
		res.render('pages/electronics', {
			cgID: "electronics-gps-units",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//   'electronics-gaming',
	app.get('/electronics-gaming', function (req, res) {
		res.render('pages/electronics', {
			cgID: "electronics-gaming",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//   'electronics-game-consoles',
	app.get('/electronics-game-consoles', function (req, res) {
		res.render('pages/electronics', {
			cgID: "electronics-game-consoles",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	//   'electronic-games',
	app.get('/electronic-games', function (req, res) {
		res.render('pages/electronics', {
			cgID: "electronic-games",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[3].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'gift-certificates',
	app.get('/gift-certificates', function (req, res) {
		res.render('pages/gift-certificates', {
			cgID: "gift-certificates",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

	// 'top-seller' 
	app.get('/top-seller', function (req, res) {
		res.render('pages/top-seller', {
			cgID: "top-seller",
			headerCat: categoryJSON,
			cat: categoryJSON.categories[0],
			subCategory: categoryJSON.categories[1].categories,
			count: count,
			count2: 0,
			jsonCatIdIndexing: jsonCatIdIndexing

		});
	});

});
// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(express.static('public'));
 
app.listen(8080);